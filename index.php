<?php
	//https://github.com/slimphp/Twig-View/issues/7
	session_start();
	ob_start();
	$rootDirectory = __DIR__;
	require_once'app/config.php';
	require_once'app/idiomas/' . $cfg['sistema']['idioma'] . '/idioma.php';
	$vendor = require_once 'vendor/autoload.php';

	require_once 'rutas/rutas.php';