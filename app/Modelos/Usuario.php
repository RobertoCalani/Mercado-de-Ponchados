<?php

	namespace Modelo;

	class Usuario extends Modelo
	{
		protected $tabla = 'usuarios';

		public function encriptarCadena($cadena) {
			$salt = substr(base64_encode(openssl_random_pseudo_bytes('22')), 0, 22);
			$salt = strtr($salt, array('+' => '.'));
			$dbhash = crypt($cadena, '$2y$10$' . $salt);
			return $dbhash;
		}

		public function verificarCadena($correo, $cadena) {
        	$this->condicion('WHERE', 'correo', '=', $correo);

			if($usuario = $this->traer(1)) {
				$hash = crypt($cadena, @$usuario['clave']);			
				if($hash == @$usuario['clave']):
					return $usuario;
				else:
					return;
				endif;
			}
		}
	}