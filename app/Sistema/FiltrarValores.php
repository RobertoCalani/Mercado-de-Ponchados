<?php
	namespace Sistema;


	class FiltrarValores
	{
		public static function cargador() {
			$resultado = array();
			foreach ($_POST as $clave => $valor) {
				if(preg_match('/^\w+$/', $clave)) {
					$resultado[$clave] = trim($_POST[$clave]);
				}
			}

			foreach ($_FILES as $clave => $valor) {
				if(preg_match('/^\w+$/', $clave)) {
					if($_FILES[$clave]['tmp_name']) {
						$resultado[$clave] = $_FILES[$clave];
					}
				}
			}
			return $resultado;
		}

		public static function redireccionar($ruta)
		{
			header('Location: ' . $ruta);
			exit;
		}

		public static function crearURL($valor = NULL)
		{

			return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($valor, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
		}
		
		public static function html($valor)
		{
			return trim(nl2br(htmlentities($valor, ENT_QUOTES, "UTF-8")));
		}

		public static function traerConectado($usuario = NULL)
		{
			if($usuario) {
				$_SESSION['conectado'] = $usuario;
			} else {
				return @$_SESSION['conectado'];
			}
		}

		public static function traerAdmin($estado = NULL)
		{
			if($estado) {
				$_SESSION['conectadoAdmin'] = 'Adminstrador';
			} else {
				return @$_SESSION['conectadoAdmin'];
			}
		}
		
		public static function fecha()
		{
			return Date('Y-m-d H:i:s');
		}

		public static function ip()
		{
			return $_SERVER["REMOTE_ADDR"];
		}

		public static function leerVariblesGlobales($nombre)
		{
			$dir = __DIR__ . '/../archivos/variables/' . $nombre . '.json';
			if(!file_exists($dir)) {
				return ['error'=>'Variables no encontradas'];
			}
			$datos = file_get_contents($dir);
			$json = json_decode($datos, true);
			return $json;
		}
	}
?>