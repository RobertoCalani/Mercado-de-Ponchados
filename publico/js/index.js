$(function() { 
	moment.locale('es');
	$('.fecha').html(function() {
		return moment($(this).text()).format('LLL');
	});
	$('.btn-download').on('click', function() {
		document.location = "/descargar/" + $(this).attr('ponchado-id') + "/tipo/" + $(this).attr('tipo');
		return;
	});
});
