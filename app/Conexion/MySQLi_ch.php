<?php namespace Conexion;
	/**
	* 
	*/
	class MySQLi_ch
	{
		public $tabla;
		public $campos;
		public $condiciones;
		public $ordenes;
		public $limite;
		public $pagina_get;
		public $paginacion;

		public $valores;

		private $conexion;

		function __construct($servidor, $db, $usuario, $clave)
		{
			$this->conexion = new \mysqli($servidor, $usuario, $clave, $db);
		}

		public function traer()
		{	
			// Preparamos los campos a mostrar
			// Ejecutamos los PREPARE
			$sql = "SELECT {$this->retornarCampos('query')} FROM {$this->tabla} {$this->retornarCondiciones()} {$this->retornarOrdenes()} {$this->retornarLimites()}";	
			$stmt = $this->conexion->prepare($sql); 

			// Establecer variables condicionales
			if($this->condiciones) {
				$tipo = "";
				for ($i=0; $i < count($this->condiciones); $i++) { 
					${$this->condiciones[$i][1]} = $this->condiciones[$i][3];
					$a [] = &${$this->condiciones[$i][1]};
					$tipo .= "s";
				}

				call_user_func_array(array($stmt, "bind_param"), array_merge(array($tipo), $a));

			}

			$stmt->execute();

			// Mostrnado resultados
			$result = $stmt->get_result();

			/* Get the number of rows */
			//$num_of_rows = $result->num_rows;
			$resultados = array();
			while ($row = $result->fetch_assoc()) {
				$resultados[] = $row;
			}

			/* free results */
			$stmt->free_result();
			$stmt->close();
			
			return $resultados;
		}

		public function insertar()
		{
			$valores = [];
			$valores_r = [];
			$pregunta = [];

			$tipo = "";
			$this->a = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $key;
				$valores_r []= $value;
				$pregunta []= "?"; 

				${$this->valores[$key]} = $value;
				$this->a [] = &${$this->valores[$key]};
				$tipo .= "s";
			}
			$valores = implode(', ', $valores);
			$pregunta = implode(', ', $pregunta);
			$sql = "INSERT INTO {$this->tabla} ({$valores}) VALUES ({$pregunta})";

			$stmt = $this->conexion->prepare($sql); 		
		
			call_user_func_array(array($stmt, "bind_param"), array_merge(array($tipo), $this->a));

			$stmt->execute();
			$newId = $stmt->insert_id;
			$stmt->close();
			return $newId;
		}

		public function actualizar()
		{
			$valores = [];

			$tipo = "";
			$this->a = [];
			foreach ($this->valores as $key => $value) {
				$valores []= $key . ' = ' . '?';

				${$this->valores[$key]} = $value;
				$this->a[] = &${$this->valores[$key]};
				$tipo .= "s";
			}

			for ($i=0; $i < count($this->condiciones); $i++) { 
				/* 
					0 = WHERE
					1 = 'campo'
					2 = '=', '!='
					3 = valor
				*/
				${$this->condiciones[$i][1]} = $this->condiciones[$i][3];
				$this->a [] = &${$this->condiciones[$i][1]};
				$tipo .= "s";
			}
			//dd($a);
			$valores = implode(', ', $valores);
			$sql = "UPDATE {$this->tabla} SET {$valores} {$this->retornarCondiciones()}";

			$stmt = $this->conexion->prepare($sql); 		
		
			call_user_func_array(array($stmt, "bind_param"), array_merge(array($tipo), $this->a));

			if($stmt->execute()) {
				$stmt->close();
				return true;
			}
		}

		public function eliminar()
		{
			$sql = "DELETE FROM {$this->tabla} {$this->retornarCondiciones()}";	
			echo $sql;
			$stmt = $this->conexion->prepare($sql); 

			// Establecer variables condicionales
			if($this->condiciones) {
				$tipo = "";
				for ($i=0; $i < count($this->condiciones); $i++) { 
					${$this->condiciones[$i][1]} = $this->condiciones[$i][3];
					$a [] = &${$this->condiciones[$i][1]};
					$tipo .= "s";
				}
			
				call_user_func_array(array($stmt, "bind_param"), array_merge(array($tipo), $a));

			}						 
			if($stmt->execute()) {
				return true;
			}
		}

		public function verificar($campo, $valor)
		{
			$q = $this->conexion->query("SELECT COUNT(*) as total FROM {$this->tabla} WHERE {$campo} = '{$valor}'");
			$row = $q->fetch_row();
			return $row[0];
		}

		/*
		 |
		 |	Sub funciones
		 |
		 */
				
		public function retornarCampos($tipo = null)
		{
			return ($this->campos) ? implode(", ", $this->campos) : ' * ';
		}
		
		private function retornarCondiciones()
		{
			$sql = "";
			if(is_array($this->condiciones)) {
				/* 
					0 = WHERE
					1 = 'campo'
					2 = '=', '!='
					3 = valor
				*/
				for ($i=0; $i < count($this->condiciones); $i++) { 
					if($this->condiciones[$i][0] == 'WHERE') {

						if($i == 0) {
							$sql .= $this->condiciones[$i][0] . ' ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
						} else {
							$sql .= ' AND ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
						}
					}

					if($this->condiciones[$i][0] == 'OR') {
						$sql .= $this->condiciones[$i][0] . ' ' . $this->condiciones[$i][1] . ' ' . $this->condiciones[$i][2] . ' ?';
					}
				}
			}

			

			return $sql;
		}

		private function retornarOrdenes()
		{
			$sql = "";
			if($this->ordenes) {
				for ($i=0; $i < count($this->ordenes); $i++) { 
					$sql .= " ORDER BY " . implode(", ", $this->ordenes[$i][0]) ." ". $this->ordenes[$i][1];
				}
				return $sql;
			}
		}

		private function retornarLimites() {
			if(!$this->limite) {
				return null;
			}

			$sql = " LIMIT ";	
			if(@$_GET[$this->pagina_get] || $this->limite) {
				$sqlContar = "SELECT COUNT(*) as total FROM {$this->tabla} {$this->retornarCondiciones()}";
				$stmt = $this->conexion->prepare($sqlContar); 
				if($this->condiciones) {
					$tipo = "";
					for ($i=0; $i < count($this->condiciones); $i++) { 
						${$this->condiciones[$i][1]} = $this->condiciones[$i][3];
						$a [] = &${$this->condiciones[$i][1]};
						$tipo .= "s";
					}
				
					call_user_func_array(array($stmt, "bind_param"), array_merge(array($tipo), $a));

				}

				$stmt->execute();
				$result = $stmt->get_result();
				$registros = array();
				while ($row = $result->fetch_assoc()) {
					$registros[] = $row;
				}
				$p_get = (int)abs(ceil(@$_GET[$this->pagina_get]));
				$p_get = ($p_get < 1) ? 1 : $p_get;

				$paginas = ceil($registros[0]['total'] / $this->limite);

				$pagina = ($p_get < 1) ? 1 : $p_get;
				$pagina = ($p_get > $paginas) ? $paginas : $p_get;
				
				$desde = ($pagina == 1 || $pagina == 0) ? 0: ($pagina - 1) * $this->limite;

				$this->paginacion = [
					'paginas'	=>	$paginas,
					'parado'	=>	$pagina,
					'nombre'	=>	$this->pagina_get
				];

				$sql .= $desde . ', ' . $this->limite;
			} else {
				$sql .= $this->limite;
			}
			return $sql;
		}

		/*
		 |
		 |		Sector de mapeo de las tablas, devuelve los valores de los campos y el tipo
		 |
		 */

		private function traerNombreCampos()
		{
			$campos = $this->mapearTabla();
			$camposListo = [];
			for ($i=0; $i < count($campos); $i++) { 
				$camposListo []= $campos[$i]['Field'];
			}
			return $camposListo;
		}
		
		private function mapearTabla()
		{
			$sql = "SHOW COLUMNS FROM {$this->tabla}";

			$ejecutar = $this->conexion->query($sql);

			$arreglo = array();           
			if($ejecutar) {
				foreach ($ejecutar as $row) {
					$arreglo[] = $row;
				}
			}
			return $arreglo;
		}

		public function verificarTabla()
		{
			$sql = "SHOW TABLES LIKE '{$this->tabla}'";
			$ejecutar = $this->conexion->query($sql);

			$arreglo = array();           
			if($ejecutar) {
				foreach ($ejecutar as $row) {
					$arreglo[] = $row;
				}
			}


			if(count($arreglo) > 0) {
				return true;
			} else {
				return false;
			}
		}

		function __destruct()
		{
			$this->conexion = null;
		}
	}
?>