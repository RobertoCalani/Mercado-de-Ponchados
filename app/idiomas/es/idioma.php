<?php

	$idioma = array(
		'?'	=>	'Error desconocido, comuniquese con el administrador',
		/* Validacion */
		'requerido' =>	"El campo %s es requerido",
		'alfanumerico' =>	"El campo %s tiene que ser alfanumerico",
		'minimo' =>	"El campo %s tiene que ser mayor a %s caracteres",
		'maximo' =>	"El campo %s tiene que ser menor a %s caracteres",
		'correo' =>	"El campo %s no es un correo valido",
		'entero' =>	"El campo %s tiene que ser entero",
		'no_es_dst'	=>	'El archivo debe ser *.DST (Tajima)',
		'numerico'	 =>	'El campo %s tiene que ser numerico',
		'no_es_imagen'	 =>	'El campo %s no es un archivo de imagen',
		/* Formularios */
		'captcha_incorrecto' =>	"El captcha ingresado no es valido",
		'usuario_creado' =>	"Usuario creado correctamente",
		'error_usuario_creado' => "Hubo un error al intentar crear el usuario, por favor comuniquese con el administrador",
		'existe_correo' =>	"El correo ingresado ya existe",
		'no_existe_correo'=> "El correo ingresado no existe en nuestro sistema",
		'usuario_ingresado' => "El usuario ingreso correctamente",
		'clave_incorrecta' => "La clave ingresada no es correcta"
	);