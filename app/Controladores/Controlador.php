<?php

	namespace Controlador;
	use Twig_Autoloader;
	use Twig_Loader_Filesystem as Twig_Loader_Filesystem;
	use Twig_Environment as Twig_Environment;
	
	use Sistema\Ruta as Ruta;
	use Sistema\FiltrarValores as F;
	
	use Sistema\TwigCargador as TwigCargador;
	class Controlador
	{
		protected $twig;
		public $carpeta;
		public $directorio = 'vistas/defecto';
		public function __construct() {
			Twig_Autoloader::register();
			$loader = new Twig_Loader_Filesystem('vistas/defecto');

			$loader->addPath('vistas/defecto', 'sitio');
			$loader->addPath('vistas/vistasAdmin', 'admin');
			
			$this->twig = new Twig_Environment($loader, array(
				'cache' => /*'cache'*/ false,
				'debug' => true
			));
			// Crear las variables globales
			new TwigCargador($this->twig);
		}

		public function render($nombre, $valores = array()) {
			echo $this->twig->render($this->carpeta . '/' . $nombre . '.html', $valores);
			exit();
		}

		public function proteger() {
			if(!F::traerConectado()) {
				F::redireccionar(ruta('sitio.index'));
			}
		}

		public function protegerAdmin() {
			if(!F::traerAdmin()) {
				$r = new Ruta();
				F::redireccionar($r->ruta('admin.index'));
			}
		}
	}