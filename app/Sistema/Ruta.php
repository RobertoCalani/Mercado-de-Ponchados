<?php namespace Sistema;

class Ruta {

	/**
	 * @var array Array of all routes (incl. named routes).
	 */
	protected $routes = array();

	/**
	 * @var array Array of all named routes.
	 */
	protected $namedRoutes = array();

	/**
	 * @var string Can be used to ignore leading part of the Request URL (if main file lives in subdirectory of host)
	 */
	protected $basePath = '';

	/**
	 * Establecemos el controaldor
	 */
	protected $controlador = '';

	/**
	 * @var array Array of default match types (regex helpers)
	 */
	protected $matchTypes = array(
		'i'  => '[0-9]++',
		'a'  => '[0-9A-Za-z]++',
		'h'  => '[0-9A-Fa-f]++',
		'*'  => '.+?',
		'**' => '.++',
		''   => '[^/\.]++'
	);

	/**
	  * Create router in one call from config.
	  *
	  * @param array $routes
	  * @param string $basePath
	  * @param array $matchTypes
	  */
	public function __construct( $routes = array(), $basePath = '', $matchTypes = array() ) {
		/*$this->addRoutes($routes);
		$this->crearBase($basePath);
		$this->addMatchTypes($matchTypes);*/
	}
	
	/**
	 * Retrieves all routes.
	 * Useful if you want to process or display routes.
	 * @return array All routes.
	 */
	public function obtenerRutas() {
		return $this->routes;
	}

	/**
	 * Add multiple routes at once from array in the following format:
	 *
	 *   $routes = array(
	 *      array($method, $route, $target, $name)
	 *   );
	 *
	 * @param array $routes
	 * @return void
	 * @author Koen Punt
	 * @throws Exception
	 */
	public function addRoutes($routes){
		if(!is_array($routes) && !$routes instanceof Traversable) {
			throw new \Exception('Routes should be an array or an instance of Traversable');
		}
		foreach($routes as $route) {
			call_user_func_array(array($this, 'map'), $route);
		}
	}

	/**
	 * Set the base path.
	 * Useful if you are running your application from a subdirectory.
	 */
	public function crearBase($base) {

		$this->basePath = $base;
		$baseActual = json_encode(['base'	=>	$base]);
		$baseEscrita = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR . 'archivos'.DIRECTORY_SEPARATOR.'base.json');
		

		
		if (strcmp($baseActual, $baseEscrita) !== 0){
			file_put_contents(__DIR__.DIRECTORY_SEPARATOR. '..'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR.'base.json', $baseActual);
		}
	}

	public function crearControlador($controlador) {
		$this->controlador = $controlador;
	}
	/**
	 * Add named match types. It uses array_merge so keys can be overwritten.
	 *
	 * @param array $matchTypes The key is the name and the value is the regex.
	 */
	public function addMatchTypes($matchTypes) {
		$this->matchTypes = array_merge($this->matchTypes, $matchTypes);
	}

	/**
	 * Map a route to a target
	 *
	 * @param string $method One of 5 HTTP Methods, or a pipe-separated list of multiple HTTP Methods (GET|POST|PATCH|PUT|DELETE)
	 * @param string $route The route regex, custom regex must start with an @. You can use multiple pre-set regex filters, like [i:id]
	 * @param mixed $target The target where this route should point to. Can be anything.
	 * @param string $name Optional name of this route. Supply if you want to reverse route this url in your application.
	 * @throws Exception
	 */
	public function agregarRuta($method, $route, $target, $name = null) {

		$this->routes[] = array($method, $route, $target, $name);

		if($name) {
			if(isset($this->namedRoutes[$name])) {
				throw new \Exception("Can not redeclare route '{$name}'");
			} else {
				$this->namedRoutes[$name] = $route;
			}

		}

		return;
	}

	/**
	 * Reversed routing
	 *
	 * Generate the URL for a named route. Replace regexes with supplied parameters
	 *
	 * @param string $routeName The name of the route.
	 * @param array @params Associative array of parameters to replace placeholders with.
	 * @return string The URL of the route with named parameters in place.
	 * @throws Exception
	 */
	public static function ruta($routeName, array $params = array()) {

		$rutas = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR.'rutas.json');
		$rutas = json_decode($rutas, true);

		$base = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR.'base.json');
		$base = json_decode($base, true);
		// Check if named route exists
		if(!isset($rutas[$routeName])) {
			return 'No-Ruta:____: "'.$routeName . '"';
		}

		// Replace named parameters
		$route = $rutas[$routeName];

		// prepend base path to route url again
		$url = $base['base'] . $route;

		if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER)) {
			foreach($matches as $match) {
				list($block, $pre, $type, $param, $optional) = $match;

				if ($pre) {
					$block = substr($block, 1);
				}

				if(isset($params[$param])) {
					$url = str_replace($block, $params[$param], $url);
				} elseif ($optional) {
					$url = str_replace($pre . $block, '', $url);
				}
			}
		}
		return $url;
	}

	/**
	 * Match a given Request Url against stored routes
	 * @param string $requestUrl
	 * @param string $requestMethod
	 * @return array|boolean Array with route information on success, false on failure (no match).
	 */
	public function ejecutarMapeo($requestUrl = null, $requestMethod = null) {
			
		$this->rutasCompletas();

		$parametros = array();
		$match = false;
		/*
			$requestUrl = URL actual (Sin contar la base y separando los GET)
			TODO ESTE TRABAJO LO LO AHCE CON LA URL EN LA QUE ESTAMOS PARADOS
		*/ 
		// Establece la ur si es que no tiene, si no tiene establece / por defecto
		if($requestUrl === null) {
			$requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
		}
		// Quita la base si es que lo tiene
		$requestUrl = substr($requestUrl, strlen($this->basePath));

		// Separa la url que queres de los get
		if (($strpos = strpos($requestUrl, '?')) !== false) {
			$requestUrl = substr($requestUrl, 0, $strpos);
		}
		// Establece el tipo de METODO (GET, POST, PUT, DELETE, ETC)
		if($requestMethod === null) {
			$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
		}


		/**
		 *	Hace un foreach a la lista de rutas para comparar una por una y asi ver cual corresponde a *  la ruta en la que estamos parados
		 */
		foreach($this->routes as $partesDeLaRuta) {

			// Verificamos y escribimos las rutas
			$this->leerRutas();

			// Separamos en variables el contenido del array de rutas
			list($metodo, $ruta, $accion, $nombre) = $partesDeLaRuta;

			$method_match = (stripos($metodo, $requestMethod) !== false);

			// Si ve que el metodo (cabecera) no coincide con el actual, es decir en el que estamos parado entonces apra a la siguiente vuelta del foreach
			if (!$method_match) continue;
			if ($ruta === '*') {
				// *vale todo
				$match = true;

			} elseif (isset($ruta[0]) && $ruta[0] === '@') {
				// Aca definimos cuestras expresiones regulares propias
				$pattern = '`' . substr($ruta, 1) . '`u';
				$match = preg_match($pattern, $requestUrl, $parametros) === 1;
			} elseif (($position = strpos($ruta, '[')) === false) {
				// Si una ruta NO NO NO, repito NO  contiene parametros entonces tendra 0 coincidencias
				$match = strcmp($requestUrl, $ruta) === 0;
			} else {
				// Compara la url actual con la url establecida sin la variable [x]
				if (strncmp($requestUrl, $ruta, $position) !== 0) {
					continue;
				}
				$regex = $this->compileRoute($ruta);
				$match = preg_match($regex, $requestUrl, $parametros) === 1;
			}

			if ($match) {

				if ($parametros) {
					foreach($parametros as $key => $value) {
						if(is_numeric($key)) unset($parametros[$key]);
					}
				}

				/*return array(
					'target' => $accion,
					'params' => $parametros,
					'name' => $nombre
				);*/
				if(is_object($accion)) {
					call_user_func_array( $accion, $parametros);
					exit();
				} else {
					$controlador = ($this->controlador) ? $this->controlador . '\\' . $accion : $accion;
					$controlador = explode('|', $controlador);

					if(class_exists($controlador[0])) {
						$c = new $controlador[0]();
						if(method_exists($controlador[0],$controlador[1])){
							call_user_func_array(array(new $controlador[0], $controlador[1]), array_values($parametros));
						} else {
							// no existe metodo
						}
					}					
				}
			break; // Una vez que encontro una ruta entonces deja de buscar
			}
		}
		// No encontro rutas
		return false;
	}

	/**
	 * Compile the regex for a given route (EXPENSIVE)
	 */
	private function compileRoute($route) {

		if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER)) {



			// Guarda en $mathchTypes las expresiones regulares
			$matchTypes = $this->matchTypes;

			foreach($matches as $match) {

				/*				
					[0] => /[i:id]		$block
				    [1] => /			$pre
				    [2] => i 			$type
				    [3] => id 			$param
				    [4] => 
				 */
				list($block, $pre, $type, $param, $optional) = $match;

				

				if (isset($matchTypes[$type])) {
					$type = $matchTypes[$type];
				}
				
				if ($pre === '.') {
					$pre = '\.';
				}

				//Older versions of PCRE require the 'P' in (?P<named>)
				$pattern = '(?:'
						. ($pre !== '' ? $pre : null)
						. '('
						. ($param !== '' ? "?P<$param>" : null)
						. $type
						. '))'
						. ($optional !== '' ? '?' : null);

				$route = str_replace($block, $pattern, $route);
			}

		}

		return "`^$route$`u";
	}

	private function leerRutas()
	{
		$rutasActuales = json_encode($this->namedRoutes);

		$rutasEscritas = file_get_contents(variables('directorio') . '/app/archivos/rutas.json');

		if (strcmp($rutasActuales, $rutasEscritas) !== 0){
			file_put_contents(variables('directorio') . '/app/archivos/rutas.json', $rutasActuales);
		}
		return $rutasEscritas;
	}

	public function rutasCompletas($estado = null)
	{
		$rutasActuales = json_encode($this->routes);
		file_put_contents(variables('directorio') . '/app/archivos/rutasCompletas.json', $rutasActuales);

		if($estado) {
			$l = file_get_contents(variables('directorio') . '/app/archivos/rutasCompletas.json');
			return json_decode($l, true);
		}
	}

	/*public static function comprobarRuta($nombre = null)
	{
		$l = file_get_contents(variables('directorio') . '/app/archivos/rutasCompletas.json');
		$rutas = json_decode($l, true);

		for ($i=0; $i < count($rutas); $i++) { 
			if($rutas[$i][3] == $nombre) {
				$estado = true;
			}
		}

		return (@$estado) ? $estado : false;
	}*/
}
