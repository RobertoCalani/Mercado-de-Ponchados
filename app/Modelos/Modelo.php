<?php

	namespace Modelo;	
	//use Conexion\SQLSrv_ch as SQLSrv_ch;
	use Conexion\MySQLi_ch as MySQLi_ch;

	class Modelo
	{
		/**
		 * Datos de sentencias SQL
		 * @var [Condiciones y la conexcion]
		 */
		
		public $campos;
		public $condiciones;
		public $ordenes;
		public $limite;

		private $conexion;

		// Valores de campos
		public $valores = [];

		public function __construct($valores = null)
		{
			global $cfg;

			$this->servidor = ($valores['servidor']) ? $valores['servidor'] : $cfg['conexion']['servidor'];
			$this->db 		= ($valores['db']) ? $valores['db'] : $cfg['conexion']['db'];
			$this->usuario 	= ($valores['usuario']) ? $valores['usuario'] : $cfg['conexion']['usuario'];
			$this->clave 	= ($valores['clave']) ? $valores['clave'] : $cfg['conexion']['clave'];

			// Conexion abierta;
			$this->Conexion = new MySQLi_ch($this->servidor, $this->db, $this->usuario, $this->clave);

			$this->verificarTabla();
		}

		private function verificarTabla()
		{
			$this->Conexion->tabla = $this->tabla;
			if(!$this->Conexion->verificarTabla()) {
				mensajeDeError('La tabla: <strong>"'.$this->tabla.'"</strong> no existe');
				exit;
			}
		}

		public function condicion($tipo = null, $campo = null, $condicion = null, $valor = null)
		{
			$this->condiciones []= [$tipo, $campo, $condicion, $valor];
		}

		public function orden($campos = [], $orden = null)
		{
			$this->ordenes []= [$campos, $orden];
		}

		public function traer($limite = null, $campos = null, $pagina = null)
		{
			$this->campos = (is_array($campos)) ? $campos : null;
			$this->limite = ($limite) ? $limite : '';

			$this->Conexion->tabla 			= $this->tabla;
			$this->Conexion->pagina_get 	= ($pagina) ? $pagina : null;
			$this->Conexion->condiciones 	= $this->condiciones;
			$this->Conexion->ordenes 		= $this->ordenes;
			$this->Conexion->campos 		= $this->campos;
			$this->Conexion->limite 		= $this->limite;

			$resultado = $this->Conexion->traer();
			$this->reset();

			/*if(count($resultado) == 1) {
				$this->asignarValores($resultado[0]);
			}*/

			return (count($resultado) == 1 && ($limite == 1)) ? $resultado[0] : $resultado;
		}

		/*private function asignarValores($registro)
		{
			foreach ($registro as $key => $value) {
				$this->valores[$key] = $value;
			}
		}*/

		public function insertar()
		{
			$this->Conexion->valores = $this->valores;
			$this->Conexion->tabla 	 = $this->tabla;

			$val = $this->Conexion->insertar();
			$this->reset();
			return $val;
		}

		public function actualizar($id = null)
		{
			$this->Conexion->condiciones 	= ($id) ? [['WHERE', 'id', '=', $id]] : $this->condiciones;
			$this->Conexion->valores 		= $this->valores;
			$this->Conexion->tabla 	 		= $this->tabla;
			$val = $this->Conexion->actualizar();
			$this->reset();
			return $val;
		}

		public function eliminar()
		{
			$this->Conexion->condiciones 	= $this->condiciones;
			$this->Conexion->tabla 	 		= $this->tabla;
			$this->reset();
			return $this->Conexion->eliminar();
		}
		
		private function reset()
		{
			$this->condiciones = [];
			$this->ordenes = [];
		}

		public function verificar($campo, $valor) {
			$this->Conexion->condiciones 	= $this->condiciones;
			$this->Conexion->tabla = $this->tabla;
			return $this->Conexion->verificar($campo, $valor);
		}

		public function paginacion()
		{
			return $this->Conexion->paginacion;
		}

		function __destruct()
		{
			$this->tabla = null;
		} 
	}