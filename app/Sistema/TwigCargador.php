<?php
	namespace Sistema;
	use Twig_SimpleFunction as Twig_SimpleFunction;
	use Sistema\FiltrarValores as F;
	//use Sistema\Seo as Seo;

	class TwigCargador
	{
		private $twig;

		public function __construct($twig)
		{
			$this->twig = $twig;
			$this->cargadorGlobales();
			$this->cargadorFunciones();
			$this->leerFunciones();
		}
		private function cargadorGlobales()
		{
			global $cfg;
			$this->twig->addGlobal('publico', $cfg['sitio']['protocolo'] . '://' . $_SERVER['HTTP_HOST'] . '/' . $cfg['sistema']['base'] . '/publico');
			$this->twig->addGlobal('base', $cfg['sitio']['protocolo'] . '://' . $_SERVER['HTTP_HOST'] . '/' . $cfg['sistema']['base']);
			$this->twig->addGlobal('sitio', $cfg['sitio']['protocolo'] . '://' . $_SERVER['HTTP_HOST']);
			$this->twig->addGlobal('llave_publica', $cfg['captcha']['publico']);
			$this->twig->addGlobal('conectado', @$_SESSION['conectado']);
			$this->twig->addGlobal('conectadoAdmin', @$_SESSION['conectadoAdmin']);
			$this->twig->addGlobal('carrito', @$_SESSION['carro']);
			$this->twig->addGlobal('F',new FiltrarValores());
		}
		private function leerFunciones()
		{
			// https://stackoverflow.com/questions/13277444/using-a-custom-function-in-twig
			/*$FiltrarValores = get_class_methods('\Sistema\FiltrarValores');
			for ($i=0; $i < count($FiltrarValores); $i++) {


				
				$funcion = new Twig_SimpleFunction('{$FiltrarValores[$i]}', function($tipo = null) {

					$nombre = (!$tipo)? 'flash' : $tipo;

					(isset($_SESSION[$nombre]))?$listo = $_SESSION[$nombre]:$listo=null;
					unset($_SESSION[$nombre]);
					$_SESSION[$nombre] = null;
					return $listo;

				});
				$this->twig->addFunction($funcion);
			}*/
		}
		private function cargadorFunciones()
		{
			$funcion = new Twig_SimpleFunction('flash', function($nombre = 'flash') {

				
				(isset($_SESSION[$nombre]))?$listo = $_SESSION[$nombre]:$listo=null;
				unset($_SESSION[$nombre]);
				$_SESSION[$nombre] = null;
				return $listo;

			});
			$this->twig->addFunction($funcion);

			$funcion = new Twig_SimpleFunction('anterior', function() {
				return $_SERVER['HTTP_REFERER'];
			});
			$this->twig->addFunction($funcion);

			$funcion = new Twig_SimpleFunction('dd', function($array = null) {

				return dd($array);

			});
			$this->twig->addFunction($funcion);

			$funcion = new Twig_SimpleFunction('ruta', function($nombre, array $valores = array()) {
				return ruta($nombre, $valores);
			});
			$this->twig->addFunction($funcion);

			$funcion = new Twig_SimpleFunction('seo', function($necesita, $titulo = null, $descripcion = null, $imagen = null) {

				$Seo = new Seo();
				return $Seo->iniciar($necesita, $titulo, $descripcion, $imagen);

			});
			$this->twig->addFunction($funcion);
					
			$funcion = new Twig_SimpleFunction('paginacion', function($valor = null) {
				if(!$valor) {
					return '';
				}
				
				if($valor['paginas'] == 1 || $valor['paginas'] == 0) {
					return '';
				}

				$listo = parse_url($_SERVER["REQUEST_URI"]);
				$ruta = $listo['path'] . '?';
				//echo $listo['query'] . '<br/>';
				
				$argumentos = explode('&',@$listo['query']);
				//echo $argumentos[0] . '<br/>';
				
				$url = [];
				
				for ($i=0; $i < count($argumentos); $i++) {
					$parte = explode('=', $argumentos[$i]);
					
					if($parte[0] != $valor['nombre']) {
						$url []= ($parte[0]) ? $parte[0] . '=' . $parte[1] : null ; 
					}
				}
				$url = implode('&', $url);
				$ruta = $ruta . $url . '&' . $valor['nombre'] . '=';
				$parado = $valor['parado'];
				

				$atras = ($parado <= 1) ? null : $ruta . ($parado - 1);
				$adelante = ($parado >= $valor['paginas']) ? null : $ruta . ($parado + 1);

				$primero = ($parado != 1) ? $ruta . 1 : null;
				$ultimo = ($parado != $valor['paginas']) ? $ruta . $valor['paginas'] : null;
				
				$valor_atras_p = $valor['paginas'] - 4;
				
				$atras_p = ($parado > 5);
				$adelante_p = ($parado < $valor_atras_p);
				
				$atras_3 = ((($parado - 3) > 0) && (($parado - 3) != 1)) ? $ruta . ($parado - 3) : null ;
				$atras_2 = ((($parado - 2) > 0) && (($parado - 2) != 1)) ? $ruta . ($parado - 2) : null ;
				$atras_1 = ((($parado - 1) > 0) && (($parado - 1) != 1)) ? $ruta . ($parado - 1) : null ;
				
				$adelante_1 = ((($parado + 1) < $valor['paginas']) && (($parado + 1) != $valor['paginas'])) ? $ruta . ($parado + 1) : null ;
				
				$adelante_2 = ((($parado + 2) < $valor['paginas']) && (($parado + 2) != $valor['paginas'])) ? $ruta . ($parado + 2) : null ;
				
				$adelante_3 = ((($parado + 3) < $valor['paginas']) && (($parado + 3) != $valor['paginas'])) ? $ruta . ($parado + 3) : null ;

				
				$html = "<nav><ul class='pagination'>";
				$html .= ($atras) ? "<li><a href='{$atras}'><span aria-hidden='true'>&laquo;</span></a></li>" : '' ;
				$html .= ($primero) ? "<li><a href='{$primero}'>1</a></li>" : '' ;
				
				$html .= ($atras_p) ? "<li><span>...</span></li>" : "";
				
				$html .= ($atras_3) ? "<li><a href='{$atras_3}'>".($parado - 3)."</a></li>" : "";
				$html .= ($atras_2) ? "<li><a href='{$atras_2}'>".($parado - 2)."</a></li>" : "";
				$html .= ($atras_1) ? "<li><a href='{$atras_1}'>".($parado - 1)."</a></li>" : "";
				
				$html .= "<li class='active'><span>{$parado}</span></li>";
				
				$html .= ($adelante_1) ? "<li><a href='{$adelante_1}'>".($parado + 1)."</a></li>" : "";
				$html .= ($adelante_2) ? "<li><a href='{$adelante_2}'>".($parado + 2)."</a></li>" : "";
				$html .= ($adelante_3) ? "<li><a href='{$adelante_3}'>".($parado + 3)."</a></li>" : "";	
				
				$html .= ($adelante_p) ? "<li><span>...</span></li>" : "";
				
				$html .= ($ultimo) ? "<li><a href='{$ultimo}'>{$valor['paginas']}</a></li>" : '' ;
				$html .= ($adelante) ? "<li><a href='{$adelante}'><span aria-hidden='true'>&raquo;</span></a></li>" : '' ;
				$html .= "</ul></nav>";
				
				return ($valor['paginas'] > 0) ? $html : null ;
			});
			$this->twig->addFunction($funcion);
		}
	}
?>